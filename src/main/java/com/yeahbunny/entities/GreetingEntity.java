package com.yeahbunny.entities;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */
public class GreetingEntity {

    private long id;
    private String content;

    public GreetingEntity() {
        
    }

    public GreetingEntity(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public String getContent() {
        return content;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
