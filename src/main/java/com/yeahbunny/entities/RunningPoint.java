package com.yeahbunny.entities;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */
public class RunningPoint {
    private Float latitude;
    private Float longitude;

    public RunningPoint() {

    }

    public RunningPoint(Float lat, Float lon) {
        latitude = lat;
        longitude = lon;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }
}
