package com.yeahbunny.entities;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */
public class ImportStatus {

    private String stauts;

    public ImportStatus() {

    }

    public ImportStatus(String status) {
        this.stauts = status;
    }

    public String getStauts() {
        return stauts;
    }

    public void setStauts(String stauts) {
        this.stauts = stauts;
    }
}
