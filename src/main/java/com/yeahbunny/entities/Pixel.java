package com.yeahbunny.entities;


import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */
@Entity
public class Pixel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "pixel_id")
    private Long id;

    private Integer latitude;
    private Integer longitude;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Pixel() {

    }

    public Pixel(Integer latitude, Integer longitude, User user) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.user = user;
    }

    public Pixel(float latitude, float longitude, User user) {
        this.latitude = Math.round(latitude);
        this.longitude = Math.round(longitude);
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getLatitude() {
        return latitude;
    }

    public void setLatitude(Integer latitude) {
        this.latitude = latitude;
    }

    public Integer getLongitude() {
        return longitude;
    }

    public void setLongitude(Integer longitude) {
        this.longitude = longitude;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Pixel{" +
                "id=" + id +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", user=" + user.toString() +
                '}';
    }
}
