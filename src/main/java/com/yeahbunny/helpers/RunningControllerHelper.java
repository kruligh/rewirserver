package com.yeahbunny.helpers;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.yeahbunny.entities.Pixel;
import com.yeahbunny.entities.RunningPoint;
import com.yeahbunny.entities.User;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */
public class RunningControllerHelper {

    private static final float STEP = 5f;

    public static Polygon getPolygonFromRunningPoints(List<RunningPoint> points) {
        int counter = 0;
        float[] vertices = new float[points.size()*2]; //2 spaces for each vertex

        for(RunningPoint p : points) {
            int xVert = truncateFloat(p.getLongitude());
            int yVert = truncateFloat(p.getLatitude());
            vertices[counter++] = xVert;
            vertices[counter++] = yVert;
        }

        return new Polygon(vertices);
    }

    private static int truncateFloat(Float f) {
        f *= 10000;
        return 5*Math.round(f/5);
    }

    public static List<Rectangle> splitRectangle(Rectangle rect) {
        List<Rectangle> outputRectangles = new LinkedList<>();
        for(float i = rect.getX(); i < rect.getX() + rect.getWidth(); i+=STEP) {
            for(float j = rect.getY(); j < rect.getY() + rect.getHeight(); j += STEP) {
                outputRectangles.add(new Rectangle(i, j, STEP, STEP));
            }
        }

        return outputRectangles;
    }

    public static boolean rectInsidePolygon(Rectangle r, Polygon p) {
        float x = r.getX();
        float y = r.getY();

        float width = r.getWidth();
        float height = r.getHeight();

        if(!p.contains(x, y))
            return false;
        if(!p.contains(x+width, y+height))
            return false;
        if(!p.contains(x+width, y))
            return false;
        if(!p.contains(x, y+height))
            return false;

        return true;
    }

    public static List<Pixel> getPixelsFromRectangles(List<Rectangle> rectangles, User u) {
        List<Pixel> outputList = rectangles.stream().map(r -> new Pixel(r.getY(), r.getX(), u)).collect(Collectors.toList());

        return outputList;
    }
}
