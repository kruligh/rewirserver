package com.yeahbunny.controllers.util;

import com.yeahbunny.entities.User;
import com.yeahbunny.security.CustomUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class PrincipalProvider {

    public User getPersonEntity(){
        return ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getPersonEntity();
    }
}

