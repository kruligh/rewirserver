package com.yeahbunny.controllers;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.yeahbunny.controllers.util.PrincipalProvider;
import com.yeahbunny.entities.ImportStatus;
import com.yeahbunny.entities.Pixel;
import com.yeahbunny.entities.RunningPoint;
import com.yeahbunny.entities.User;
import com.yeahbunny.helpers.RunningControllerHelper;
import com.yeahbunny.repositories.PixelRepository;
import com.yeahbunny.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Iterator;
import java.util.List;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */

@RestController
@RequestMapping("/rest/running")
public class RunningController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PixelRepository pixelRepository;

    @Autowired
    PrincipalProvider principalProvider;

    @RequestMapping(value = "import", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ImportStatus importStatus(@RequestBody List<RunningPoint> pointList,
                                     @RequestParam(name = "userid") String userid) {

        //create bounding box
        //iterate through all squares within box
        //  for every square check if all the points are within the box
        //      if all points within the box -> update square with new color
        //if everything went ok -> reutrn success
       // User user = userRepository.findById(Long.decode(userid));
        User user = userRepository.findById(principalProvider.getPersonEntity().getId());

        Polygon runningPolygon = RunningControllerHelper.getPolygonFromRunningPoints(pointList);
        Rectangle boundingBox = runningPolygon.getBoundingRectangle();

        List<Rectangle> rectangleList = RunningControllerHelper.splitRectangle(boundingBox);


        for(Iterator<Rectangle> it = rectangleList.iterator(); it.hasNext();) {
            Rectangle r = it.next();

            if(!RunningControllerHelper.rectInsidePolygon(r, runningPolygon))
                it.remove();
        }

        List<Pixel> pixels = RunningControllerHelper.getPixelsFromRectangles(rectangleList, user);

        user.setPoints(user.getPoints() + pixels.size());
        userRepository.save(user);

        for(Iterator<Pixel> it = pixels.iterator(); it.hasNext();) {
            Pixel px = it.next();

            Pixel px2 = pixelRepository.findByLatitudeAndLongitude(px.getLatitude(), px.getLongitude());
            if(px2 != null) {
                pixelRepository.updateUser(px.getLatitude(), px.getLongitude(), user.getId());
                it.remove();
            }
        }

        pixelRepository.save(pixels);


        return new ImportStatus("SUCCESS");
    }
}
