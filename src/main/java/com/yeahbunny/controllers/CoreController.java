package com.yeahbunny.controllers;

import com.yeahbunny.controllers.dto.LoginRequest;
import com.yeahbunny.controllers.util.PrincipalProvider;
import com.yeahbunny.entities.GreetingEntity;
import com.yeahbunny.entities.User;
import com.yeahbunny.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */
@RestController
@RequestMapping("/rest/core")
public class CoreController {

    private final AtomicLong counter = new AtomicLong();

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PrincipalProvider principalProvider;

    @Autowired
    UserRepository userRepository;

    @RequestMapping("greeting")
    public GreetingEntity greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new GreetingEntity(counter.incrementAndGet(), String.format("Hello %s!", name));
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> login(@RequestBody LoginRequest loginRequest) {

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(loginRequest.getLogin(), loginRequest.getPassword());
        Authentication a = authenticationManager.authenticate(token);

        SecurityContextHolder.getContext().setAuthentication(a);
        return ResponseEntity.ok().body(RequestContextHolder.currentRequestAttributes().getSessionId());
    }

    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public void logout (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
    }

    @RequestMapping(value = "/getMyUser", method = RequestMethod.GET)
    public @ResponseBody User getMyUser (HttpServletRequest request, HttpServletResponse response) {
        User user = userRepository.findOne(principalProvider.getPersonEntity().getId());

        return user;
    }
}
