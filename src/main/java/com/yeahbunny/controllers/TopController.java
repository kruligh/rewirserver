package com.yeahbunny.controllers;

import com.yeahbunny.entities.User;
import com.yeahbunny.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */
@RestController
@RequestMapping("/rest/top")
public class TopController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping("/getSeason")
    public List<User> getSeason(@RequestParam(value = "limit", defaultValue = "10") String li) {
        int limit = Integer.parseInt(li);
        Page<User> userPage = userRepository.getTop(new PageRequest(0, limit));
        List<User> users = new LinkedList<>();

        for(User u : userPage) {
            users.add(u);
        }

        return users;
    }
}
