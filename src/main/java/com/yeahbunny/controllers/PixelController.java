package com.yeahbunny.controllers;

import com.yeahbunny.entities.Pixel;
import com.yeahbunny.repositories.PixelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */
@RestController
@RequestMapping("/rest/pixel")
public class PixelController {

    @Autowired
    PixelRepository pixelRepository;

//    @RequestMapping("getAll")
//    public Iterable<Pixel> getAllPixels() {
//        return pixelRepository.findAll();
//    }

    @RequestMapping("get")
    public Pixel getByLatitudeAndLongitude(@RequestParam("latitude") String latitude,
                                                 @RequestParam("longitude") String longitude) throws Exception {
        return pixelRepository.findByLatitudeAndLongitude(
                Integer.decode(latitude), Integer.decode(longitude));
    }

    @RequestMapping("getAll")
    public List<Pixel> getAllByUserId(@RequestParam("userid") String userid) {
        return pixelRepository.findAllByUserId(Long.decode(userid));
    }

    @RequestMapping("getMap")
    public List<Pixel> getMap(@RequestParam("top") String top,
                              @RequestParam("bottom") String bottom,
                              @RequestParam("left") String left,
                              @RequestParam("right") String right) {

/*        return pixelRepository.findLatLongBetween(
                Math.round(Float.parseFloat(top)*10000),
                Math.round(Float.parseFloat(bottom)*10000),
                Math.round(Float.parseFloat(left)*10000),
                Math.round(Float.parseFloat(right)*10000)
        );*/
        return pixelRepository.findLatLongBetween(Integer.parseInt(top),
                Integer.parseInt(bottom),
                Integer.parseInt(left),
                Integer.parseInt(right));

    }


}
