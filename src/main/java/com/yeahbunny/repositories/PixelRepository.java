package com.yeahbunny.repositories;

import com.yeahbunny.entities.Pixel;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */
public interface PixelRepository extends CrudRepository<Pixel, Long> {
    public Pixel findByLatitude(Integer latitude);

    public Pixel findByLongitude(Integer longitude);

    public List<Pixel> findAllByUserId(Long userId);

//    @Query("select p from Pixel p where p.latitude=?1 AND p.longitude=?2")
//    public Pixel findByLatLon(Float latitude, Float longitude);

    public Pixel findByLatitudeAndLongitude(Integer latitude, Integer longitude);

    @Query("select p from Pixel p where (p.latitude between ?2 and ?1) and (p.longitude between ?3 and ?4)")
    public List<Pixel> findLatLongBetween(Integer top, Integer bottom, Integer left, Integer right);

    @Modifying
    @Transactional
    @Query("update Pixel p SET p.user.id=:userid WHERE p.latitude=:latitude AND p.longitude=:longitude")
    public void updateUser(@Param("latitude") Integer latitude, @Param("longitude") Integer longitude, @Param("userid")Long user_id);
}
