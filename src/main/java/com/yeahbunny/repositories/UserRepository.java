package com.yeahbunny.repositories;

import com.yeahbunny.entities.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Piotr Papaj on 17.12.2016.
 */
public interface UserRepository extends CrudRepository<User, Long>{

    User findByUsername(String username);
    User findById(Long id);

    @Query("select u FROM User u ORDER BY u.points DESC")
    Page<User> getTop(Pageable pageable);
}
