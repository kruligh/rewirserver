package com.yeahbunny.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;

import java.util.Arrays;

@Configuration
public class AuthenticationMenagerConfig {

    @Bean
    @Autowired
    AuthenticationManager userAuthenticationManager(CustomUserDetailsService customUserDetailsService){

        DaoAuthenticationProvider userAuthProvider = new DaoAuthenticationProvider();
        userAuthProvider.setUserDetailsService(customUserDetailsService);
        return new ProviderManager(Arrays.asList(userAuthProvider));
    }
}
