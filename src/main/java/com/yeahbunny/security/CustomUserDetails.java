package com.yeahbunny.security;


import com.yeahbunny.entities.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.List;

public class CustomUserDetails extends org.springframework.security.core.userdetails.User{

    private User user;

    public CustomUserDetails(User entity, List<GrantedAuthority> roles) {
        super(entity.getEmail(),entity.getPassword(), roles);
        this.user = entity;
    }

    public User getPersonEntity() {
        return this.user;
    }
}
