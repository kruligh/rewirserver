package com.yeahbunny.security;

import com.yeahbunny.entities.User;
import com.yeahbunny.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        User entity = userRepository.findByUsername(login);

        if(entity == null){
            throw new UsernameNotFoundException("USER_NOT_FOUND");
        }

       List<GrantedAuthority> roles = new ArrayList<>();
        roles.add(new SimpleGrantedAuthority("user"));
        return new CustomUserDetails(entity, roles);
    }


}